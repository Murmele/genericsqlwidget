add_executable(conductorWidget
	main.cpp
	mainwindow.cpp
	mainwindow.h
	mainwindow.ui
	conductorsqlwidget.cpp
	sqlcheckboxeditwidget.cpp
	sqlcoloreditwidget.cpp
	sqltexteditwidget.cpp
	sqleditdummywidget.cpp
	colordelegate.cpp
	columnnamedialog.cpp
)
target_link_libraries(conductorWidget PRIVATE SQLWidget Qt5::Widgets)
target_compile_definitions(conductorWidget PRIVATE DATABASEPATH="${CMAKE_CURRENT_SOURCE_DIR}/../Testdatabases/TestDatabase.sqlite3")
