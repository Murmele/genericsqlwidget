#include "sqleditdummywidget.h"

#include <QHBoxLayout>
#include <QLabel>

SqlEditDummyWidget::SqlEditDummyWidget(QWidget *parent): SqlEditWidget(parent) {
    QHBoxLayout* dummyHBox = new QHBoxLayout();
    dummyHBox->addWidget(new QLabel(tr("No edit of multiple columns supported or no edit for selected column is implemented."), this));
    setLayout(dummyHBox);
}

void SqlEditDummyWidget::setWidgetValue(const QVariant &value) {
}
