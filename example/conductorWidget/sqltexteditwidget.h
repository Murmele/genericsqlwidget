#ifndef SQLTEXTEDITWIDGET_H
#define SQLTEXTEDITWIDGET_H

#include "sqleditwidget.h"

class QLineEdit;

class SqlTextEditWidget: public SqlEditWidget
{
public:
    SqlTextEditWidget(QWidget* parent = nullptr);
private:
	void setWidgetValue(const QVariant &value) override;
private:
    QLineEdit* m_text;
};

#endif // SQLTEXTEDITWIDGET_H
