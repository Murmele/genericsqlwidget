#include "colordelegate.h"

#include <QPainter>

namespace {
    int colorIconWidth = 20;
    QString appearanceColorColumn = "AppearanceColor";
    QString appearanceColor2Column = "AppearanceSecondaryColor";
}

ColorDelegate::ColorDelegate(QObject *parent): QItemDelegate(parent)
{

}

void ColorDelegate::paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const {
    QStyleOptionViewItem opt = option;
    drawBackground(painter, opt, index);

    QString column_name = index.model()->headerData(index.column(), Qt::Orientation::Horizontal).toString();
    if (column_name == appearanceColorColumn || column_name == appearanceColor2Column) {
        QColor color;
        color.setNamedColor(index.data(Qt::DisplayRole).toString());
        // show color badge
        painter->save();

        int x = opt.rect.x() + opt.rect.width() - colorIconWidth;
        int y = opt.rect.y();

        painter->fillRect(x, y, colorIconWidth, opt.rect.height(), QBrush(color));

        opt.rect.adjust(0, 0, -colorIconWidth, 0);
        painter->restore();
    }


    QItemDelegate::paint(painter, opt, index);
}
