#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QPushButton;
class ConductorSqlWidget;
class QStackedWidget;
class SqlColorEditWidget;
class SqlEditWidget;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
	void addColumn();
	void tableSelectionChanged(const int columnIndex, const QVariant& firstValue);
	void addEditWidget(SqlEditWidget* widget, int index);

private:
    Ui::MainWindow *ui;
	QPushButton* m_addRow;
	QPushButton* m_removeRow;
	QPushButton* m_addColumn;
	QPushButton* m_removeColumn;
	ConductorSqlWidget* m_sqlWidget;
	QStackedWidget* m_edit;
	SqlColorEditWidget* m_color;
};
#endif // MAINWINDOW_H
