#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QDir>
#include <QHBoxLayout>
#include <QPushButton>
#include <QStackedWidget>
#include <QSplitter>

#include "sqlwidget.h"
#include "conductorsqlwidget.h"
#include "columnnamedialog.h"
#include "sqleditdummywidget.h"
#include "sqlcoloreditwidget.h"
#include "sqlcheckboxeditwidget.h"
#include "sqltexteditwidget.h"

namespace  {
	int dummyWidgetIndex = 0;
	int colorEditWidgetIndex = 1;
	int checkBoxEditWidgetIndex = 2;
	int textEditWidgetIndex = 3;

	const QString appearanceColorColumn = "AppearanceColor";
	const QString appearanceColor2Column = "AppearanceSecondaryColor";
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	QHBoxLayout* hBox = new QHBoxLayout();
	m_addRow = new QPushButton(tr("add Row"), this);
	m_removeRow = new QPushButton(tr("remove selected Row(s)"), this);
	m_addColumn = new QPushButton(tr("add Column"), this);
	m_removeColumn = new QPushButton(tr("remove selected Column(s)"), this);
	hBox->addWidget(m_addRow);
	hBox->addWidget(m_removeRow);
	hBox->addWidget(m_addColumn);
	hBox->addWidget(m_removeColumn);

	QVBoxLayout* vBox = new QVBoxLayout();
	vBox->addLayout(hBox);

	m_sqlWidget = new ConductorSqlWidget(this);
	QString path = DATABASEPATH;
	QDir dir(path);
	path = dir.absolutePath();
	QString errorString = m_sqlWidget->loadTable(dir.absolutePath(), "Conductors");
	if (!errorString.isEmpty()) {
		// Error occured during loading the table
	}

	// Define stacked widget which holds all widgets which are used to edit the table
	m_edit = new QStackedWidget(this);
	// dummy widget
	// This shows information when no item is selected or over multiple columns
	SqlEditDummyWidget* dummy = new SqlEditDummyWidget(this);
	m_edit->addWidget(dummy);
	m_edit->setCurrentIndex(0);

	m_color = new SqlColorEditWidget(this);
	addEditWidget(m_color, colorEditWidgetIndex);
	addEditWidget(new SqlCheckBoxEditWidget(this), checkBoxEditWidgetIndex);
	addEditWidget(new SqlTextEditWidget(this), textEditWidgetIndex);

	QSplitter* splitter = new QSplitter(this);
	splitter->addWidget(m_sqlWidget);
	splitter->addWidget(m_edit);

	vBox->addWidget(splitter);

	connect(m_addRow, &QPushButton::clicked, m_sqlWidget, &SqlWidget::addRecord);
	connect(m_removeRow, &QPushButton::clicked, m_sqlWidget, &SqlWidget::removeRecords);
	connect(m_addColumn, &QPushButton::clicked, this, &MainWindow::addColumn);
	connect(m_removeColumn, &QPushButton::clicked, m_sqlWidget, &SqlWidget::removeColumns);
	connect(m_sqlWidget, &SqlWidget::selectionChanged, this, &MainWindow::tableSelectionChanged);
	auto cw = new QWidget(this);
	cw->setLayout(vBox);

	setCentralWidget(cw);
}

void MainWindow::addColumn() {
	// determine new unique Column name
	auto columns = m_sqlWidget->columnNames();
	ColumnNameDialog dialog(columns, this);
	if (dialog.exec() != QDialog::Accepted)
		return;

	m_sqlWidget->addColumn(dialog.columnName());
}

void MainWindow::tableSelectionChanged(const int columnIndex, const QVariant& firstValue) {
	// Switch edit widget depending on the selected column
	if (columnIndex == -1) {
		m_edit->setCurrentIndex(0);
		return;
	}

	QString header = m_sqlWidget->headerData(columnIndex);
	if (header == appearanceColorColumn || header == appearanceColor2Column)
		m_edit->setCurrentIndex(colorEditWidgetIndex);
	else if (header == "Type")
		m_edit->setCurrentIndex(checkBoxEditWidgetIndex);
	else {
		QString t = m_sqlWidget->type(columnIndex);
		if (t == "TEXT") {
			m_edit->setCurrentIndex(textEditWidgetIndex);
		} else if (t == "REAL") {
			m_edit->setCurrentIndex(dummyWidgetIndex);
		}
	}

	static_cast<SqlEditWidget*>(m_edit->currentWidget())->setValue(firstValue);
	m_edit->currentWidget()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	m_edit->adjustSize();
}

void MainWindow::addEditWidget(SqlEditWidget* widget, int index) {
	m_edit->insertWidget(index, widget);
	// otherwise the widget was appended and so the index does not match anymore
	assert(m_edit->indexOf(widget) == index);

	connect(widget, &SqlEditWidget::widgetValueChanged, [this] (const QVariant& value) {
		m_sqlWidget->setValue(value);
	});
}

MainWindow::~MainWindow()
{
    delete ui;
}

