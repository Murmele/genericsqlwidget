#include "sqltexteditwidget.h"

#include <QLineEdit>
#include <QHBoxLayout>

SqlTextEditWidget::SqlTextEditWidget(QWidget *parent)
{
    m_text = new QLineEdit(this);

    QHBoxLayout* hbox = new QHBoxLayout();
    hbox->addWidget(m_text);
    setLayout(hbox);

    connect(m_text, &QLineEdit::textChanged, this, [=] (const QString value) {
        QVariant temp = value;
        valueChanged(temp);
    });
}

void SqlTextEditWidget::setWidgetValue(const QVariant& value) {
    m_text->setText(value.toString());
}
