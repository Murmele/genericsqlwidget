#ifndef COLORDELEGATE_H
#define COLORDELEGATE_H

#include <QItemDelegate>

/*!
 * \brief The ColorDelegate class
 * Custom Delegate to show color badges next to the color code in specific columns
 */
class ColorDelegate: public QItemDelegate
{
public:
    ColorDelegate(QObject *parent = nullptr);

    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;
};

#endif // COLORDELEGATE_H
