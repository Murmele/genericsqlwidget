#include "conductorsqlwidget.h"

#include "colordelegate.h"
#include "extendedtableview.h"

ConductorSqlWidget::ConductorSqlWidget(QWidget *parent): SqlWidget(parent)
{
	// Add custom delegate to show a color badge next to specific columns
	// The columns which get the badge have the columnnames AppearanceColor
	// and AppearanceSecondaryColor
    setViewDelegate(new ColorDelegate(view()));
}
