#ifndef SQLCOLOREDITWIDGET_H
#define SQLCOLOREDITWIDGET_H

#include "sqleditwidget.h"

class QLineEdit;

/*!
 * \brief The SqlColorEditWidget class
 * Widget for changing the color of values in the sql table
 */
class SqlColorEditWidget : public SqlEditWidget
{
    Q_OBJECT
public:
    SqlColorEditWidget(QWidget* parent = nullptr);
    //virtual void valueChanged(QVariant& value) = 0;
private:
	void setWidgetValue(const QVariant &value) override;

private:
    QLineEdit* m_color;
};

#endif // SQLCOLOREDITWIDGET_H
