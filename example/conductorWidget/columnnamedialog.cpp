#include "columnnamedialog.h"
#include "ui_columnnamedialog.h"

#include <QPushButton>

ColumnNameDialog::ColumnNameDialog(QStringList &columnNames, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ColumnNameDialog),
    m_columnNames(columnNames)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(false);
    connect(ui->le_columnName, &QLineEdit::textChanged, this, &ColumnNameDialog::columnNameChanged);
}

QString ColumnNameDialog::columnName() {
    return ui->le_columnName->text();
}

ColumnNameDialog::~ColumnNameDialog() {
    delete ui;
}

/*!
 * \brief ColumnNameDialog::columnNameChanged
 * Determine new unique columnname
 * \param columnName
 */
void ColumnNameDialog::columnNameChanged(const QString& columnName) {

    bool ok = true;

    if (columnName != "") {
        for (auto column: m_columnNames) {
            if (columnName == column) {
                ok = false;
                break;
            }
        }
    } else {
        ok = false;
    }

    ui->buttonBox->button(QDialogButtonBox::StandardButton::Ok)->setEnabled(ok);
}
