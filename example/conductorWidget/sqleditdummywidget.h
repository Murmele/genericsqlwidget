#ifndef SQLEDITDUMMYWIDGET_H
#define SQLEDITDUMMYWIDGET_H

#include "sqleditwidget.h"

class SqlEditDummyWidget : public SqlEditWidget
{
public:
    SqlEditDummyWidget(QWidget* parent = nullptr);
private:
	void setWidgetValue(const QVariant& value) override;
};

#endif // SQLEDITDUMMYWIDGET_H
