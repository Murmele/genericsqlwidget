#include "sqlcheckboxeditwidget.h"

#include <QCheckBox>
#include <QVariant>
#include <QHBoxLayout>

SqlCheckBoxEditWidget::SqlCheckBoxEditWidget(QWidget *parent): SqlEditWidget(parent)
{
    m_check = new QCheckBox(this);
    m_check->setTristate(false);

    QHBoxLayout* hbox = new QHBoxLayout();
    hbox->addWidget(m_check);
    setLayout(hbox);

    connect(m_check, &QCheckBox::stateChanged, this, [=] (const int value) {
        QVariant temp;
        if (value == Qt::CheckState::Checked)
            temp = 1;
        else
            temp = 0;
        valueChanged(temp);
    });
}

void SqlCheckBoxEditWidget::setWidgetValue(const QVariant &value) {

    if (value.toInt() == 1)
        m_check->setChecked(Qt::CheckState::Checked);
    else
        m_check->setCheckState(Qt::CheckState::Unchecked);
}
