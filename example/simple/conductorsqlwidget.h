#ifndef CONDUCTORSQLWIDGET_H
#define CONDUCTORSQLWIDGET_H

#include "sqlwidget.h"

class SqlColorEditWidget;

class ConductorSqlWidget : public SqlWidget
{
public:
    ConductorSqlWidget(QWidget* parent = nullptr);
    int changeEditWidget(QModelIndexList& itms) override;
private:
    SqlColorEditWidget* m_color;
};

#endif // CONDUCTORSQLWIDGET_H
