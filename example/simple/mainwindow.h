#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "sqlwidget.h"

class QPushButton;

class TableWidget: public SqlWidget {
	virtual int changeEditWidget(QModelIndexList& itms) override {

	}
};

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
	void addColumn();

private:
    Ui::MainWindow *ui;
	QPushButton* m_addRow;
	QPushButton* m_removeRow;
	QPushButton* m_addColumn;
	QPushButton* m_removeColumn;
	TableWidget* m_sqlWidget;
};
#endif // MAINWINDOW_H
