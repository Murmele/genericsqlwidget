#ifndef COLORDELEGATE_H
#define COLORDELEGATE_H

#include <QItemDelegate>

/*!
 * \brief The ColorDelegate class
 * Delegate to show a color
 */
class ColorDelegate: public QItemDelegate
{
public:
    ColorDelegate(QObject *parent = nullptr);

    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;
};

#endif // COLORDELEGATE_H
