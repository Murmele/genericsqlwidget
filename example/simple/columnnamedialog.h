#ifndef COLUMNNAMEDIALOG_H
#define COLUMNNAMEDIALOG_H

#include <QDialog>

namespace Ui {
class ColumnNameDialog;
}

class ColumnNameDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ColumnNameDialog(QStringList& columnNames, QWidget *parent = nullptr);
    ~ColumnNameDialog();
    QString columnName();

private slots:
    void columnNameChanged(const QString &columnName);

private:
    Ui::ColumnNameDialog *ui;
    QStringList& m_columnNames;
};

#endif // COLUMNNAMEDIALOG_H
