#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDir>
#include <QHBoxLayout>
#include <QPushButton>

#include "sqlwidget.h"
#include "columnnamedialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	QHBoxLayout* hBox = new QHBoxLayout();
	m_addRow = new QPushButton(tr("add Row"), this);
	m_removeRow = new QPushButton(tr("remove selected Row(s)"), this);
	m_addColumn = new QPushButton(tr("add Column"), this);
	m_removeColumn = new QPushButton(tr("remove selected Column(s)"), this);
	hBox->addWidget(m_addRow);
	hBox->addWidget(m_removeRow);
	hBox->addWidget(m_addColumn);
	hBox->addWidget(m_removeColumn);

	QVBoxLayout* vBox = new QVBoxLayout();
	vBox->addLayout(hBox);

	m_sqlWidget = new SqlWidget(this);
	QString path = DATABASEPATH;
	QDir dir(path);
	path = dir.absolutePath();
	QString errorString = m_sqlWidget->loadTable(dir.absolutePath(), "Conductors");
	if (!errorString.isEmpty()) {
		// Error occured during loading the table
	}
	vBox->addWidget(m_sqlWidget);

	connect(m_addRow, &QPushButton::clicked, m_sqlWidget, &SqlWidget::addRecord);
	connect(m_removeRow, &QPushButton::clicked, m_sqlWidget, &SqlWidget::removeRecords);
	connect(m_addColumn, &QPushButton::clicked, this, &MainWindow::addColumn);
	connect(m_removeColumn, &QPushButton::clicked, m_sqlWidget, &SqlWidget::removeColumns);

	auto cw = new QWidget(this);
	cw->setLayout(vBox);

	setCentralWidget(cw);
}

void MainWindow::addColumn() {
	// sql library needed, because qt does not support
	//d->model->insertColumn(m_view->model()->rowCount());

	// determine new unique Column name
	auto columns = m_sqlWidget->columnNames();
	ColumnNameDialog dialog(columns, this);
	if (dialog.exec() != QDialog::Accepted)
		return;

	m_sqlWidget->addColumn(dialog.columnName());
}

MainWindow::~MainWindow()
{
    delete ui;
}

