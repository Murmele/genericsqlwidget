#ifndef SQLWIDGETCOMMANDS_H
#define SQLWIDGETCOMMANDS_H

#include <QUndoCommand>
#include <QSqlRecord>
#include <QVariant>

class SqlTableModel;
class QTableView;
typedef QList<QModelIndex> QModelIndexList;
class QSqlDatabase;

class AddRecordCommand: public QUndoCommand {
public:
	AddRecordCommand(SqlTableModel* model):
		m_model(model) {}

	void redo() override;
	void undo() override;


private:
	int m_row{-1};
	SqlTableModel* m_model;
	QSqlRecord m_record;
};

class RemoveRecordCommand: public QUndoCommand {
public:
	typedef struct {
		int row;
		QSqlRecord record;
	} Record;

	RemoveRecordCommand(QTableView* view, SqlTableModel* model, QModelIndexList& idxList);
	void redo() override;
	void undo() override;

private:
	QTableView* m_view;
	SqlTableModel* m_model;
	QList<Record> m_records;
};

class AddColumnCommand: public QUndoCommand {
public:
	AddColumnCommand(QTableView* view, SqlTableModel* model, QSqlDatabase* db, QString tableName, QString columName, QString columnType);

	void redo() override;
	void undo() override;
private:
	QString m_table;
	QString m_name;
	QString m_type;
	QSqlDatabase* m_db;
	SqlTableModel* m_model;
	QTableView* m_view;
};

class RemoveColumnsCommand: public QUndoCommand {
public:
	RemoveColumnsCommand(QTableView* view, SqlTableModel* model,
						 QSqlDatabase* db, QString tableName,
						 QStringList columnNames);

	void redo() override;
	void undo() override;
	bool success();
	QString error();

private:
	QString m_table;
	QStringList m_columns;
	QStringList m_types;
	QSqlDatabase* m_db;
	QString m_err{""};
	QList<QList<QVariant>> m_entries;
	QList<int> m_ids;
	SqlTableModel* m_model;
	QTableView* m_view;
	/*!
	 * Indicates if redo was successful or not.
	 * If not successful, m_err contains the error message
	 */
	bool m_success{false};
};


class SetValueCommand: public QUndoCommand {
public:
	SetValueCommand(QVariant newValue, QTableView* view,
					SqlTableModel* model,
					QModelIndexList& selectedIndexes);
	void undo() override;

	void redo() override;
private:
	QList<QVariant> m_old;
	QVariant m_new;
	QTableView* m_table;
	SqlTableModel* m_model;
	QModelIndexList m_selectedIndexes;
};

#endif // SQLWIDGETCOMMANDS_H
