#ifndef EXTENDEDTABLEVIEW_H
#define EXTENDEDTABLEVIEW_H

#include <QTableView>

class FilterTableHeader;

class ExtendedTableView : public QTableView
{
    Q_OBJECT
public:
    ExtendedTableView(QWidget* parent = nullptr);

    FilterTableHeader* filterHeader() { return m_header; }
    void setModel(QAbstractItemModel *model);

private:
    FilterTableHeader* m_header{nullptr};
};

#endif // EXTENDEDTABLEVIEW_H
