#include "sqlwidget.h"

#include "sqlwidgetprivate.h"
#include "extendedtableview.h"
#include "filtertableheader.h"
#include "sqltablemodel.h"


#include <QSortFilterProxyModel>
#include <QLabel>
#include <QAbstractItemModel>
#include <QUndoStack>
#include <QShortcut>
#include <QSqlField>
#include <QVBoxLayout>
#include <QSqlError>

#include "sqlwidgetcommands.h"

SqlWidget::SqlWidget(QWidget *parent):
    QWidget(parent),
    d(new SqlWidgetPrivate())
{
    m_undoStack = new QUndoStack(this);

    m_view = new ExtendedTableView(this);
    m_view->setModel(d->model);
    m_view->show();
    // do not edit the table directly, but use its own widget
    m_view->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QVBoxLayout* vBox = new QVBoxLayout();
    vBox->addWidget(m_view);
    QWidget* viewWidget = new QWidget(this);
    viewWidget->setLayout(vBox);
	setLayout(vBox);

    initConnection();
}

void SqlWidget::initConnection() {
    QShortcut* shortcut = new QShortcut(QKeySequence(tr("Ctrl+z")), this);
    connect(shortcut, &QShortcut::activated, this, [=] {m_undoStack->undo();});
    shortcut = new QShortcut(QKeySequence(tr("Ctrl+y")), this);
    connect(shortcut, &QShortcut::activated, this, [=] {m_undoStack->redo();});

    connect(m_view->selectionModel(), &QItemSelectionModel::selectionChanged, this, &SqlWidget::updateEditWidget);

    // Set up filters
    connect(m_view->filterHeader(), &FilterTableHeader::filterChanged, this, &SqlWidget::updateFilter);
}

QString SqlWidget::loadTable(QString database_path, QString table, QStringList requiredColumns) {
    d->db.close();

    d->db.setDatabaseName(database_path);
    if (!d->db.isValid())
        qDebug("Database not valid");

    d->table = table;

    // TODO: create new table when not existing

	if (!d->db.open())
		return d->db.lastError().text();

    d->model->setTable(table);
    d->model->select(); // populate data

    m_view->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    const bool show = true;
    if (!show)
        m_view->hideColumn(0); // don't show the ID
    qobject_cast<FilterTableHeader*>(m_view->horizontalHeader())->generateFilters(d->model->columnCount(), show);
	return "";
}

void SqlWidget::setViewDelegate(QAbstractItemDelegate* delegate) {
    m_view->setItemDelegate(delegate);
}

QString SqlWidget::headerData(int column) const {
    return d->model->headerData(column, Qt::Horizontal, Qt::DisplayRole).toString();
}

QString SqlWidget::type(int column) const {
    return QVariantTypeToSqlType(d->model->record().field(column).type());
}

ExtendedTableView* SqlWidget::view() const
{
    return m_view;
}

void SqlWidget::addRecord() {
    // TODO: do not execute again before the record was added correctly
    // How to check?
    m_undoStack->push(new AddRecordCommand(d->model));
}

void SqlWidget::removeRecords() {
    // QSortFilterProxyModel indexes
    QModelIndexList idxs = m_view->selectionModel()->selectedIndexes();
    m_undoStack->push(new RemoveRecordCommand(m_view, d->model, idxs));
}

QString SqlWidget::QVariantTypeToSqlType(QVariant::Type type) {
    if (type == QVariant::Type::String)
        return "TEXT";
    if (type == QVariant::Type::Double)
        return "REAL";
    if (type == QVariant::Type::Int)
        return "INTEGER";
    if (type == QVariant::Type::ByteArray)
        return "BLOB";
    // NUMERIC
    assert(false);
}

QStringList SqlWidget::columnNames() {
	QSqlRecord record = d->db.record(d->table);
	QStringList columns;

	for (int i=0; i < record.count(); i++) {
		columns << record.fieldName(i);
	}
	return columns;
}

void SqlWidget::addColumn(const QString& name) {
	const QString type = "TEXT";
	m_undoStack->push(new AddColumnCommand(m_view, d->model, &d->db, d->table, name, type));
}

void SqlWidget::removeColumns() {
    QModelIndexList list = m_view->selectionModel()->selectedIndexes();

    if (list.isEmpty())
        return;

    QSqlRecord record = d->db.record(d->table);
    QStringList columnNames;

    // check that column is only one time added, because
    // when multiple rows of a column are selected,
    // the column should be deleted only once ;)
    for (QModelIndex idx: list) {
        int idxColumn = idx.column();
        bool exists = false;
        QString columnName = record.fieldName(idxColumn);
        for (auto column: columnNames) {
            if (column == columnName) {
                exists = true;
                break;
            }
        }
        if (!exists)
            columnNames << columnName;
    }

    RemoveColumnsCommand* cmd = new RemoveColumnsCommand(m_view, d->model, &d->db, d->table, columnNames);
    m_undoStack->push(cmd); // calls also redo()
    if (!cmd->success()) {
        QString error = cmd->error();
        emit errorOccured(error);
        int index = m_undoStack->index();
        m_undoStack->setIndex(index - 1);
        assert(index - 1 == m_undoStack->index());
    }
}

void SqlWidget::updateFilter(int column, const QString& value) {

    // copied from sqlbrowser,
    // check for license issues
    d->model->updateFilter(column, value);
}

void SqlWidget::setValue(const QVariant& value) {
    QModelIndexList idxs = m_view->selectionModel()->selectedIndexes();
    m_undoStack->push(new SetValueCommand(value, m_view, d->model, idxs));
}

void SqlWidget::updateEditWidget(const QItemSelection &selected, const QItemSelection &deselected) {
    Q_UNUSED(selected);
    Q_UNUSED(deselected);

    QModelIndexList itms = m_view->selectionModel()->selectedIndexes();
    if (itms.isEmpty()) {
		emit selectionChanged(-1, QVariant()); //clearEditWidget();
        return;
    }

    int column = itms[0].column();
    for (QModelIndex itm: itms) {
        if (itm.column() != column) {
		   emit selectionChanged(-1, QVariant());
           return;
        }
    }

	emit selectionChanged(column, itms.first().data(Qt::DisplayRole));
}
