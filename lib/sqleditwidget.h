#ifndef EDITWIDGET_H
#define EDITWIDGET_H

#include <QWidget>

/*!
 * \brief The SqlEditWidget class
 * Base class.
 */
class SqlEditWidget: public QWidget
{
    Q_OBJECT
public:
    SqlEditWidget(QWidget* parent = nullptr);

	void setValue(const QVariant &value);

private:
	virtual void setWidgetValue(const QVariant& value) = 0;

signals:
    /*!
     * \brief widgetValueChanged
     * Do not call this function!
     * \param value
     */
	void widgetValueChanged(const QVariant& value);
public slots:
	void valueChanged(const QVariant &value);
private:
    bool m_suppressValueChanges{false};
};

#endif // EDITWIDGET_H
