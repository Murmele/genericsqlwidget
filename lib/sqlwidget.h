#ifndef SQLWIDGET_H
#define SQLWIDGET_H

#include <QWidget>
#include <QModelIndexList>

class SqlWidgetPrivate;
class SqlEditWidget;

class ExtendedTableView;
class QItemSelection;
class QAbstractItemDelegate;
class QUndoStack;

/*!
 * \brief The SqlWidget class
 * This class is indented to be used for all Sql stuff.
 * - Use setViewDelegate to set a specific delegate
 * -
 */
class SqlWidget: public QWidget
{
    Q_OBJECT
public:
    SqlWidget(QWidget* parent=nullptr);
    /*!
     * \brief loadDatabase
     * \param path
     * \param requiredColumns Columns which are required in the database.
     *        Sometime specific information is needed and so the columns must exist
     */
	QString loadTable(QString database_path, QString table, QStringList requiredColumns = QStringList());
    /*!
     * \brief setViewDelegate
     * Use it to set a delegate to the tableView
     * \param delegate
     */
    void setViewDelegate(QAbstractItemDelegate* delegate);

    /*!
     * \brief addEditWidget
     * Use this function to add widget for editing specific values
     * See SqlColorEditWidget for example
     * TODO: be careful when adding index which is not in range!
     * \param widget
     * \param index
     */
    void addEditWidget(SqlEditWidget* widget, int index);

    /*!
     * \brief headerData
     * Returns the header data from the TableView
     * \param column
     * \return
     */
    QString headerData(int column) const;

    QString type(int column) const;

    ExtendedTableView *view() const;

    static QString QVariantTypeToSqlType(QVariant::Type type);

	/*!
	 * \brief addColumn
	 * Adds a column to the widget
	 * \param name Name of the new column
	 */
	void addColumn(const QString& name);

	/*!
	 * \brief columnNames
	 * \return column names of all columns
	 */
	QStringList columnNames();

	/*!
	 * \brief addRecord
	 * Add a new row to the table
	 * TODO: check which values are used
	 */
	void addRecord();

	/*!
	 * \brief removeRecords
	 * Removes the currently selected records
	 */
	void removeRecords();

	/*!
	 * \brief removeColumns
	 * Removes the currently selected columns
	 */
	void removeColumns();

	/*!
	 * \brief setValue
	 * Sets value \p value to the selected cells
	 * \param value
	 */
	void setValue(const QVariant &value);

signals:
    /*!
     * \brief errorOccured
     * Connect to this signal if you want to get notified when an error occured
     * \param err Error message
     */
    void errorOccured(QString& err);

	/*!
	 * \brief selectionChanged
	 * Emitted when selection changed. Column indicates the index of the column
	 * \param column the index of the selected column. If no or multiple columns are selected -1 is returned
	 * \param initValue value of the first selected cell
	 */
	void selectionChanged(const int column, const QVariant& initValue);
private:
    void updateEditWidget(const QItemSelection &selected, const QItemSelection &deselected);
    void initConnection();
    void updateFilter(int column, const QString &value);

private:
    ExtendedTableView *m_view;
    SqlWidgetPrivate* d{nullptr};
    QUndoStack* m_undoStack{nullptr};
};

#endif // SQLWIDGET_H
