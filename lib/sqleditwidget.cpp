#include "sqleditwidget.h"

SqlEditWidget::SqlEditWidget(QWidget *parent): QWidget(parent) {

}

void SqlEditWidget::setValue(const QVariant& value) {

    if (m_suppressValueChanges)
        return;

    m_suppressValueChanges = true;
    setWidgetValue(value);
    m_suppressValueChanges = false;
}

void SqlEditWidget::valueChanged(const QVariant& value) {
    if (m_suppressValueChanges)
        return;

    emit widgetValueChanged(value);
}
