#include "sqlwidgetprivate.h"

#include "sqltablemodel.h"

SqlWidgetPrivate::SqlWidgetPrivate()
{
   db = QSqlDatabase::addDatabase("QSQLITE");
   model = new SqlTableModel(nullptr, db);


   model->setEditStrategy(SqlTableModel::OnFieldChange);
   //model->setHeaderData(0, Qt::Horizontal, tr("Name")); // renaming
}

SqlWidgetPrivate::~SqlWidgetPrivate() {
   delete model;
}
