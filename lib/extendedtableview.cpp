#include "extendedtableview.h"

#include <QSortFilterProxyModel>

#include "filtertableheader.h"

ExtendedTableView::ExtendedTableView(QWidget *parent): QTableView(parent)
{
    m_header = new FilterTableHeader(this);
    setHorizontalHeader(m_header);

    setSortingEnabled(true);

    m_header->generateFilters(0, false);
}

void ExtendedTableView::setModel(QAbstractItemModel* model) {
    // sorting when pressing on column header
    QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
    m->setDynamicSortFilter(true);
    m->setSourceModel(model);

    QTableView::setModel(m);
}
