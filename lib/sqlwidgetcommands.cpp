 #include "sqlwidgetcommands.h"

#include "sqltablemodel.h"
#include "sqlwidget.h"

#include <QSqlField>
#include <QTableView>
#include <QSortFilterProxyModel>
#include <QSqlQuery>
#include <QSqlError>

#include "filtertableheader.h" // TODO: try to get rid of it!

void AddRecordCommand::undo() {
   for (int i=0; i < m_model->rowCount(); i++) {
	   if (m_record.field(m_model->IDColumnIndex()).value().toInt() == m_model->getID(i)) {
		   m_model->removeRow(i);
		   break;
	   }
   }
   m_model->submitAll();
   // otherwise an empty line stays in the model
   m_model->select();
}

void AddRecordCommand::redo() {
	if (m_row < 0) {
		// if the redo is called a second time, insert the row to a specific position
		// otherwise append it
		m_row = m_model->rowCount();
	}

	if (!m_model->insertRow(m_row))
		return;

	if (m_record.isEmpty()) {
		// Important: If a database field does not have a default value and the "not null" flag
		// is set, the data must be set manually here
		// the database does not have a default value for the name
		m_model->setDefaultName(m_row);
	} else {
		// redo was called a second time (redo - undo - redo)
		// restore the id
		m_model->setRecord(m_row, m_record);
	}

	// it is important too submit everything, because. Add column cannot be done by
	// insertColumn and must be done directly by a query. After the query a select() is
	// done and all unchanged modifications are lost
	m_model->submitAll();
	// with submitAll default values are set to the new record
	// but they are not yet in the model. So repopulate the model
	// to see the default values in the view
	m_model->select();

	m_record = m_model->record(m_row);


}

RemoveRecordCommand::RemoveRecordCommand(QTableView* view, SqlTableModel* model, QModelIndexList& idxList):
	m_view(view),
	m_model(model) {

	QList<int> rows;

	for (auto idx: idxList) {
		QSortFilterProxyModel* proxy = static_cast<QSortFilterProxyModel*>(m_view->model());
		int idxRow = proxy->mapToSource(idx).row();
		bool exists = false;
		for (int row: rows) {
			if (row == idxRow) {
				exists = true;
				break;
			}
		}
		if (!exists) {
			rows.append(idxRow);
		}
	}

	// sorting is important, because when a smaller index is remove first,
	// the upper is anymore correct
	std::sort(rows.begin(), rows.end());

	Record r;
	for (auto row: rows) {
		r.row = row;
		r.record = m_model->record(row);
		m_records.append(r);
	}


}

void RemoveRecordCommand::undo() {
	for (int i = m_records.count() - 1; i >= 0; i--) {
		m_model->insertRow(m_records[i].row);
		m_model->setRecord(m_records[i].row, m_records[i].record);
	}
	m_model->submitAll();
	m_model->select();
}

void RemoveRecordCommand::redo() {

	for (int i = m_records.count() - 1; i >= 0; i--) {
		m_model->removeRow(m_records[i].row);
	}

	m_model->submitAll();
	m_model->select();
}

bool dropDatabaseColumns(QSqlDatabase* db, QString tableName, QStringList& deleteColumns) {
// https://stackoverflow.com/questions/5938048/delete-column-from-sqlite-table/5987838#5987838
// https://stackoverflow.com/questions/8442147/how-to-delete-or-add-column-in-sqlite

   QSqlQuery query("", *db);

   QSqlRecord record = db->record(tableName);

	QString columnNames;
	for (int i = 0; i < record.count(); i++) {
		QString column = record.fieldName(i);
		QString typeStr = SqlWidget::QVariantTypeToSqlType(record.field(i).type());
		bool found = false;
		for (auto c: deleteColumns) {
		   if (c == column) {
			   found = true;
			   break;
		   }
		}

		if (!found) {
			if (i > 0)
				columnNames += ",";
			columnNames += column + " " + typeStr;
		}
	}

	const QString tableBackup = "table_backup";

	QStringList queries;
	queries << QString("PRAGMA foreign_keys=OFF");
	queries << QString("BEGIN TRANSACTION");
	queries << QString("CREATE TABLE %1(%2);").arg(tableBackup).arg(columnNames);
	queries << QString("INSERT INTO %1 SELECT %2 FROM %3;").arg(tableBackup).arg(columnNames).arg(tableName);
	queries << QString("DROP TABLE %1").arg(tableName);
	queries << QString("ALTER TABLE %1 RENAME TO %2;").arg(tableBackup).arg(tableName);
	queries << QString("COMMIT");
	queries << QString("PRAGMA foreign_keys=ON");

	QString err;
	for (auto q: queries) {
		if (!query.exec(q)) {
			err = query.lastError().text();
		}
	}
	return true;
}

AddColumnCommand::AddColumnCommand(QTableView* view, SqlTableModel* model, QSqlDatabase* db, QString tableName, QString columName, QString columnType):
m_table(tableName),
m_name(columName),
m_type(columnType),
m_db(db),
m_model(model),
m_view(view){}

void AddColumnCommand::undo() {
	QStringList names(m_name);
	dropDatabaseColumns(m_db, m_table, names);

	qobject_cast<FilterTableHeader*>(m_view->horizontalHeader())->removeFilter();

//        m_model->selectStatement();
//        m_model->setTable(m_model->tableName());
//        m_model->selectStatement();
	m_model->update();
}

void AddColumnCommand::redo() {
	QString query_ = QString("ALTER TABLE %1 ADD COLUMN %2 %3").arg(m_table).arg(m_name).arg(m_type);
	QSqlQuery query("", *m_db);
	bool status = query.exec(query_);

	if (!status) {
		QString err = query.lastError().text();
		//emit errorOccured(err);
	} else {
		qobject_cast<FilterTableHeader*>(m_view->horizontalHeader())->addFilter();
	}

	m_model->update();
//        QString q = query.lastQuery();
//        query.finish();
//        q = query.lastQuery();
//        QSqlRecord r = query.record();
//        m_model->selectStatement();
//        m_model->setQuery(query);
//        QString e = m_model->lastError().text();
//        m_model->selectStatement();
//        m_model->setTable(m_model->tableName());
//        m_model->selectStatement();
}

RemoveColumnsCommand::RemoveColumnsCommand(QTableView* view, SqlTableModel* model, QSqlDatabase* db, QString tableName, QStringList columnNames):
m_table(tableName),
m_columns(columnNames),
m_db(db),
m_model(model),
m_view(view){}

bool RemoveColumnsCommand::success() {
	return m_success;
}

QString RemoveColumnsCommand::error() {
	return m_err;
}

void RemoveColumnsCommand::undo() {

	if (!m_success)
		return;

	// restore columns
	QString query_;
	QSqlQuery query("", *m_db);
	for (int i=0; i < m_columns.length(); i++) {

		query_ = QString("ALTER TABLE %1 ADD COLUMN %2 %3").arg(m_table).arg(m_columns[i]).arg(m_types[i]);

		bool status = query.exec(query_);

		if (!status) {
			m_err = query.lastError().text();
			//emit errorOccured(err);
		}
	}

	// restore entries
	QList<int>::const_iterator ids = m_ids.begin();
	for (QList<QList<QVariant>>::const_iterator row = m_entries.begin(); row != m_entries.end(); row++) {
		QString columns;

		QList<QVariant>::const_iterator column = row->begin();
		QStringList::const_iterator columnNames = m_columns.begin();
		QStringList::const_iterator type = m_types.begin();

		for (int c = 0; c < row->length(); c++) {
			if (c != 0)
				columns += ", ";
			// how to update non string types?
			QString name = *columnNames;
			columns += name + " =";
			if (*type == "TEXT" || *type == "BLOB")
				columns += "'" + column->toString() +"'";
			else if (*type == "INTEGER")
				columns += QString::number(column->toInt());
			else if (*type == "REAL")
				columns += column->toDouble();

			columnNames++;
			column++;
			type++;
		}
		query_ = QString("UPDATE %1 SET %2 WHERE ID = %3").arg(m_table).arg(columns).arg(*ids);

		if (!query.exec(query_))
			m_err = query.lastError().text();
		ids++;
	}

	for (int i=0; i < m_columns.count(); i++) {
		qobject_cast<FilterTableHeader*>(m_view->horizontalHeader())->addFilter();
	}

	m_model->update();
}

void RemoveColumnsCommand::redo() {

	// TODO: store the data inside of the column to restore
	m_types.clear();
	m_entries.clear();
	m_err = "";
	m_success = true;
	// remove columns only if they are not mandatory

	QSqlRecord record = m_db->record(m_table);

	// if the column is required, it is not possible to delete it.
	// Abort and notify the user
	for (auto column: m_columns) {

		QSqlField field = record.field(column);
		assert(field.isValid());
		if (field.requiredStatus()) {
		   m_success = false;
		   m_err = QString("Column %1 is required and cannot be deleted").arg(column);
		   m_types.clear();
		   return;
		}

		m_types << SqlWidget::QVariantTypeToSqlType(field.type());
	}

	QSqlQuery query("", *m_db);
	if (!query.exec(QString("SELECT * FROM %1").arg(m_table))) {
		QString err = query.lastError().text();
		m_err = err;
		return;
	}


	// save entries from the deleting columns to undo
	while(query.next()) {

		QList<QVariant> columnValues;
		for (int i=0; i < m_columns.length(); i++)
			columnValues << query.value(m_columns[i]);
		m_entries << columnValues;

		// the ID column must have the name "ID"!
		m_ids << query.value("ID").toInt();
		query.isSelect();
	}

	dropDatabaseColumns(m_db, m_table, m_columns);

	for (int i=0; i < m_columns.count(); i++) {
		qobject_cast<FilterTableHeader*>(m_view->horizontalHeader())->removeFilter();
	}

	m_model->update();
}

SetValueCommand::SetValueCommand(QVariant newValue, QTableView* view, SqlTableModel* model, QModelIndexList& selectedIndexes):
	m_new(newValue),
	m_table(view),
	m_model(model) {
	QSortFilterProxyModel* proxy = static_cast<QSortFilterProxyModel*>(m_table->model());
	for (auto idx: selectedIndexes) {
		QModelIndex i = proxy->mapToSource(idx);
		assert(i.isValid());
		m_selectedIndexes.append(i);
		m_old.append(m_model->data(i));
	}
}

void SetValueCommand::undo() {
   for (int i = 0; i < m_selectedIndexes.count(); i++) {
	   m_model->setData(m_selectedIndexes[i], m_old[i]);
   }
   m_model->submitAll();
}

void SetValueCommand::redo() {
	for (QModelIndex& idx: m_selectedIndexes)
		m_model->setData(idx, m_new);
	m_model->submitAll();

}
