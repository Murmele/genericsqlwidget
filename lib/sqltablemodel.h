#ifndef SQLTABLEMODEL_H
#define SQLTABLEMODEL_H

#include "QSqlTableModel"

class SqlTableModel : public QSqlTableModel
{
    Q_OBJECT
public:
    SqlTableModel(QObject *parent = nullptr, QSqlDatabase db = QSqlDatabase());
    void setQuery(QSqlQuery& query);
    void reset();
    void clearCache();
    /*!
     * \brief updateFilter
     * Parses the value from the FilterLineEdit's and applies the changes if desired
     * \param column Column index where the filter was applied
     * \param value String from the FilterLineEdit's
     * \param applyQuery
     */
    void updateFilter(int column, const QString& value, bool applyQuery = true);

    void parseFilterString(int column, const QString& value);

    //void setTable(const QString& table) override;
    QString selectStatement() const;

    void update();

    int IDColumnIndex() const;
    int NameColumnIndex() const;
    QString DefaultName() const;

    void setIDColumnIndex(int index);
    void setNameColumnIndex(int index);
    void setDefaultName(const QString& name);

    int getID(int row);
    /*!
     * \brief setDefaultName
     * Set the default name to the row \p row
     * \param row Row in which the default name should be set
     */
    void setDefaultName(int row);


private:
    /*!
     * \brief where
     * Builds the filter condition from the text parsed with parseFilterString()
     * \return
     */
    QString where();
    QString customQuery(bool withRowid);
    void buildQuery();
    /// configure for browsing results of specified query
    void setQuery(const QString& sQuery, bool dontClearHeaders = false);

    void removeCommentsFromQuery(QString& query);

private:
    // contains the parsed filter string for each column
    /*!
     * \brief m_mWhere
     * int: filter column
     * QString: parsed filter
     */
    QMap<int, QString> m_mWhere;

    /*!
     * Index of the column where the unique id is
     */
    int m_idColumnIndex{0};
    /*!
     * Index of the column where the Name of the entry is.
     */
    int m_NameColumnIndex{1};
    /*!
     * Default value of the Name column
     */
    QString m_DefaultName{tr("Default")};
};

#endif // SQLTABLEMODEL_H
